%define         debug_package %{nil}

Name:           memcached_exporter
Version:        0.14.4
Release:        1
Summary:        Exports metrics for consumption by Prometheus
License:        Apache-2.0 and MIT
URL:            https://github.com/prometheus/memcached_exporter
Source0:        https://github.com/prometheus/memcached_exporter/archive/v%{version}/%{name}-%{version}.tar.gz
# tar -xvf Source0
# run 'go mod vendor' in it
# tar -czvf memcached_exporter-vendor.tar.gz vendor
Source1:        memcached_exporter-vendor.tar.gz

BuildRequires:  promu
BuildRequires:  golang >= 1.13

%description
Exports metrics from memcached servers for consumption by Prometheus.

%prep
%autosetup -b0 -a1 -n %{name}-%{version} -p1

%build
export GOFLAGS="-mod=vendor"
promu build

%install
install -d -p %{buildroot}%{_bindir}
install -m 755 %{_builddir}/%{name}-%{version}/%{name} %{buildroot}%{_bindir}/memcached_exporter

%files
%license LICENSE
%doc README.md
%{_bindir}/memcached_exporter

%changelog
* Wed Nov 27 2024 dongjiao <dongjiao@kylinos.cn> - 0.14.4-1
- Update package to version 0.14.4
- Update dependencies to address CVE-2023-45288

* Mon Jun 24 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 0.14.3-1
- Update package to version 0.14.3
- Bump github.com/prometheus/exporter-toolkit from 0.10.0 to 0.11.0
- Bump google.golang.org/protobuf from 1.32.0 to 1.33.0

* Tue Jan 02 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 0.14.2-1
- Update package to version 0.14.2

* Wed Feb 01 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 0.10.0-1
- Update package to version 0.10.0

* Mon Jun 13 2022 lijian <lijian2@kylinos.cn> - 0.9.0-1
- Package init
